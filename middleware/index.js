
async function authent(req,res,next){
    try {
        let session = req.session.passport
        console.log(session)
        if(session){
            next()
        }else {
            res.redirect('/login')
        }
    } catch (error) {
        res.send(error)
    }
}

async function author(req,res,next){
    try {
        let session = req.session.passport
        let id = session.user
        let paramsId = req.params.id
        if(+id === +paramsId) {
            next()
        } else {
            res.redirect('/')
        }
    } catch (error) {
        res.send(error)
    }
}

module.exports = {authent, author}