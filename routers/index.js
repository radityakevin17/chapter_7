const router = require('express').Router()
const {loginPage, userPage, homePage} = require('../controller')
const {passport } = require('../helper')
const {authent, author} = require('../middleware')


router.get('/login', loginPage)
router.post('/login', passport.authenticate('local', {
    successRedirect : '/',
    failureRedirect : '/login'
}))

router.get('/', [authent], homePage)
router.get('/user:id', [authent, author], userPage)

module.exports = router